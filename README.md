# Team segfault project for ChangeCode

This is a django project and as such it uses django and python libraries.

## Install instructions
+ start virtualenv
+ pip install from requirements
+ start the dev server or use uwsgi with a proper web server

import requests
import json
from django.shortcuts import render
from django.conf import settings
from . import forms


def login(request):
    context = {}

    if request.method == 'POST':
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

            r = json.loads(cd.get('r')['Result'])
            r2 = requests.get(settings.DETAILS_URL.format(id=r['TeamId']),
                              headers={'X-Authorization': r['AuthorizationToken']}).json()
            r2 = json.loads(r2['Result'])
            context = {
                'title': 'Team Details',
                'team': r2
            }
            return render(request, 'change/details.html', context=context)

    else:
        form = forms.LoginForm()

    context.update({
        'title': 'ChangeCode Login',
        'form': form,
    })

    return render(request, 'change/form_login.html', context=context)


def register(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

    else:
        form = forms.RegisterForm()

    context = {
        'title': 'ChangeCode Register',
        'form': form,
    }

    return render(request, 'change/form_register.html', context=context)

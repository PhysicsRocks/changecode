import requests
from django.conf import settings
from django import forms


class LoginForm(forms.Form):
    teamname = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # add css to fields
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def clean(self):
        cd = super().clean()

        if {'teamname', 'password'} != set(cd.keys()):
            return cd

        r = requests.post(settings.LOGIN_URL, json=cd).json()
        if r['Errors']:
            raise forms.ValidationError(', '.join(r['Errors']))

        cd['r'] = r
        return cd


class RegisterForm(forms.Form):
    teamname = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for i in range(4):
            self.fields['name_%d' % i] = forms.CharField()
            self.fields['surname_%d' % i] = forms.CharField()
            self.fields['email_%d' % i] = forms.EmailField()

        # add css to fields
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def clean(self):
        cd = super().clean()

        reqs = {'teamname', 'password'}
        reqs |= {'name_%d' % i for i in range(4)}
        reqs |= {'surname_%d' % i for i in range(4)}
        reqs |= {'email_%d' % i for i in range(4)}

        if set(cd.keys()) != reqs:
            return cd

        json_request = {
            'Teamname': cd.get('teamname'),
            'Password': cd.get('password'),
            'members': []
        }

        for i in range(4):
            json_request['members'] += [{
                'name': cd.get('name_%d' % i),
                'surname': cd.get('surname_%d' % i),
                'mail': cd.get('email_%d' % i),
            }]

        r = requests.post(settings.REGISTER_URL, json=json_request).json()
        if r['Errors']:
            raise forms.ValidationError(', '.join(r['Errors']))

        cd['r'] = r
        return cd
